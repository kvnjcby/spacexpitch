Thank you all for coming. I'm really excited to be here today!
My name is Kevin Jacoby. I'm from Raleigh, North Carolina and I graduated
from UNC Chapel Hill in May with a degree in computer science and a minor in
Neuroscience.

I really enjoyed the programming interview and I'm excited to share my
experience with you and talk about a few things I would do differently
in hindsight.


Before the interview:
- set up Git repo
- tested Python environment

1pm:
- frantically refreshed email
- read the problem requirements
- began mentally planning order to tackle problems
- noted that csv data needed to be ingested before anything else could work
- noted that csv columns would be in arbitrary order

- created test .csv file with provided demo data
- created Pokedex class and wrote first method to read csv file from args
- read header column names and created map for column id for easy assignment

- began to work out data structures
- first thought was to implement 1 dict with pokeID as key
- but realized that printing the requested pokemon info, finding evolutions, etc would be more efficient with a different design
- recognized that weaknesses would be less computationally complex than strengths
  - why?
- created nametoId and idtoName maps
- created pokemon types dict, weaknesses dict


- recognized that implementing DFS evolution tree method would likely be most complex, so I waited to do this until everything else had been completed
- began writing print_pokemon method
- knocked out low hanging fruit - print pokemon ID
- moved on to weaknesses next
- then strengths
- ** talk about the data structures used for these ^
- took a bathroom break and thought about life (strengths/weaknesses, evolution dfs)
- wrote evolution DFS
- tested code and submitted

What would I do differently?
- PEP8 method names
- more complete documentation
- iterative DFS method
- use sets instead of lists in defaultdicts
- reduce data structure complexity