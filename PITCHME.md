### Kevin Jacoby
#### SpaceX Interview Presentation
#### 17 July 2018

---

### Build a Pokédex

- Given a .csv file with Pokémon data and the name of a Pokémon, print:
  - Pokémon's ID
  - Pokémon it is strong against
  - Pokémon it is weak against
  - Every possible evolution path

---

### First Steps

- Divided problem into 3 primary tasks:
    1. Read and store Pokémon data from .csv
    2. Find Pokémon's strengths & weaknesses
    3. Find Pokémon's evolution paths
- Set time goals for each task |
  - Ensure enough time for testing and feedback

---

### Data Ingestion

- Created test .csv file with provided demo data
- .csv columns are ordered arbitrarily

---

##### test.csv

```
ID,Name,Types,Weaknesses,Evolution
004,Charmander,Fire,"Ground,Rock,Water",005
005,Charmeleon,Fire,"Ground,Rock,Water",006
006,Charizard,"Fire,Flying","Rock,Electric,Water",
043,Oddish,"Grass,Poison","Fire,Flying,Ice,Psychic",044
044,Gloom,"Grass,Poison","Fire,Flying,Ice,Psychic","045,182"
045,Vileplume,"Grass,Poison","Fire,Flying,Ice,Psychic",
182,Bellossom,Grass,"Bug,Fire,Flying,Ice,Poison",
```

---

### Expected Output

```text
$ ./pokedex test.csv oddish
ID:
    043
Strong against:
    Bellossom
Weak against:
    Charmander
    Charmeleon
    Charizard
Evolution:
    Oddish > Gloom > Vileplume
    Oddish > Gloom > Bellossom
```

---

#### class Pokedex(object):

```python
    def __init__(self, csvPath):
        """Initializes new Pokedex object.

        Args:
            csvPath: path to .csv file
        """
        with open(csvPath) as csvFile:
            reader = csv.reader(csvFile)

            header = next(reader)
            columns = {column.lower(): i for i, column in enumerate(header)}
```

@[11](Handle arbitrary column order by mapping column name to index)

---

### Data Organization

- Bidirectional hash map for Pokémon IDs and names
- Pokémon types
- Pokémon type weaknesses
- List of evolution edges (Pokémon IDs) for DFS paths

---

### Pokémon Strengths & Weaknesses

- To find strengths, find all Pokémon who are weak against given Pokémon's type(s)
- To find weaknesses, find all Pokémon of types that given Pokémon is weak against

---

```python
self.nameToId = {}
self.idToName = {}
self.pokeTypes = defaultdict(set)
self.types = defaultdict(set)
self.weaknesses = defaultdict(set)
self.evolutions = defaultdict(set)

for row in reader:
    pokeID = row[columns['id']]
    name = row[columns['name']].lower()

    self.nameToId[name] = pokeID
    self.idToName[pokeID] = name.capitalize()

    self.pokeTypes[pokeID].update(row[columns['types']].split(','))

    for type in self.pokeTypes[pokeID]:
        self.types[type].update([pokeID])

    self.weaknesses[pokeID].update(row[columns['weaknesses']].split(','))
     
    self.evolutions[pokeID].update(row[columns['evolution']].split(','))
```

@[3-6](Originally used defaultdict(list))
@[12-13](Name/ID maps)
@[15](Pokémon types with ID as key)
@[17-18](Pokémon types with type as key)
@[20](Weaknesses)
@[22](Evolution edges)

---

### Evolution Path DFS

<div class="left">
![Evolution Tree](dfstree.png)
</div>


<div class="right">
<pre>self.evolutions</pre>
<table>
  <thead>
    <tr>
      <th>key</th>
      <th>value</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>'043'</td>
      <td>['044']</td>
    </tr>
    <tr>
      <td>'044'</td>
      <td>['045', '182']</td>
    </tr>
    <tr>
      <td>'045'</td>
      <td>[' ']</td>
    </tr>
    <tr>
      <td>'182'</td>
      <td>[' ']</td>
    </tr>
  </tbody>
</table>

<br />

<span>043 > 044 > 045<br />
043 > 044 > 182</span>
</div>

---

### Recursive DFS

```python
    def find_evolutions(self, pokeID, path=[], paths=[]):
        """Uses recursive DFS to find all possible evolution paths for a given Pokemon.

        Args:
            pokeID: ID of Pokemon
        Returns:
            list of lists of paths containing Pokemon IDs
        """
        newPath = path + [pokemon]
        for evolution in self.evolutions[pokemon]:
            if evolution == '':
                paths.append(newPath)
            else:
                self.find_evolutions(evolution, path=newPath, paths=paths)
        return sorted(paths)
```
@[11-12](No further evolution in path; add new path to list of paths)
@[13-14](Else, recursively find all possible continuations of current path)
@[15](Sort evolution paths by Pokemon ID)

---

### Pokédex Output

---

```python
    def find_pokemon(self, pokemon):
        """Finds Pokemon in Pokedex and prints its following properties:
        ID, Strong against, Weak against, Evolution.

        Args:
            pokemon: case-insensitive name of Pokemon
        """
        pokeID = self.nameToId.get(pokemon.lower(), None)
        if pokeID is None:
            print 'Unknown Pokemon'
            return

        strengths = set()
        for type in self.pokeTypes[pokeID]:
            strengths.update([key for key, val in self.weaknesses.iteritems()
                                  if type in val])

        weaknesses = set()
        for weakness in self.weaknesses[pokeID]:
            weaknesses.update(self.types[weakness])
```

@[13-16](Find all Pokémon IDs that are weak against Pokémon's type(s))
@[18-20](Find all Pokémon IDs that are strong against Pokémon's type(s))

---

```python
        print 'ID:'
        print '    ' + pokeID

        print 'Strong against:'
        self.poke_print(strengths)

        print 'Weak against:'
        self.poke_print(weaknesses)

        print 'Evolution:'
        evolutions = self.find_evolutions(pokeID)
        for evolution in evolutions:
            self.poke_print(evolution, evolution=True)
```

---

```python
    def poke_print(self, pokeList, evolution=False):
        """Prints list of Pokemon sorted by ID and indented by 4 spaces.

        Args:
            pokeList: list of Pokemon IDs to be printed
            evolution(False): whether or not Pokemon are part of an evolution
        """
        TAB = '    '
        if evolution:
            print TAB + ' > '.join(self.idToName[pokeID] for pokeID in pokeList)
        else:
            if len(pokeList) == 0:
                print TAB + 'None'
            else:
                for pokemon in sorted(pokeList):
                    print TAB + self.idToName[pokemon]
```

---

### Output

```text
$ ./pokedex test.csv oddish
ID:
    043
Strong against:
    Bellossom
Weak against:
    Charmander
    Charmeleon
    Charizard
Evolution:
    Oddish > Gloom > Vileplume
    Oddish > Gloom > Bellossom
```

---

### In Hindsight
- Follow PEP 8 convention for method names
- Reorganize data structures
- Use iterative depth-first search

---

### Reorganized Data Structures

```python
self.nameToId = {}
self.pokemon = defaultdict()
self.types = defaultdict(set)
self.typeWeaknesses = defaultdict(set)

for row in reader:
    pokeID = row[columns['id']]
    name = row[columns['name']].lower()

    self.nameToId[name] = pokeID

    self.pokemon[pokeID] = {
        'name': name.capitalize(),
        'types': set(row[columns['types']].split(',')),
        'weaknesses': set(row[columns['weaknesses']].split(',')),
        'evolutions': set(row[columns['evolution']].split(','))
    }

    for type in self.pokemon[pokeID]['types']:
        self.types[type].update([pokeID])

    for type in self.pokemon[pokeID]['weaknesses']:
        self.typeWeaknesses[type].update([pokeID])
```
@[12-17](Store all data with pokeID as key in 1 dict)
@[22-23](Store weaknesses with weak type as key and set of IDs as value)

---

```python
    def find_pokemon(self, pokemon):
        """Finds Pokemon in Pokedex and prints its following properties:
        ID, Strong against, Weak against, Evolution.

        Args:
            pokemon: case-insensitive name of Pokemon
        """
        pokeID = self.nameToId.get(pokemon.lower(), None)
        if pokeID is None:
            print 'Unknown Pokemon'
            return

        pokemonData = self.pokemon[pokeID]

        strengths = set()
        for type in pokemonData['types']:
            # strengths.update([key for key, val in self.weaknesses.iteritems()
            #                       if type in val])
            strengths.update(self.typeWeaknesses[type])

        weaknesses = set()
        for weakness in pokemonData['weaknesses']:
            weaknesses.update(self.types[weakness])
```

@[15-19](List comprehension is no longer necessary to find strengths)

---

### Iterative DFS

```python
    def iter_find_evolutions(self, pokeID):
        """Uses iterative DFS to find all possible evolution paths for a given Pokemon.

        Args:
            pokeID: ID of Pokemon
        Returns:
            list of lists of paths containing Pokemon IDs
        """
        stack = [(pokeID, [pokeID])]
        paths = []
        while stack:
            (currentPokemon, path) = stack.pop()
            for nextPokemon in self.pokemon[currentPokemon]['evolutions']:
                if nextPokemon == '':
                    paths.append(path)
                else:
                    stack.append((nextPokemon, path + [nextPokemon]))
        return sorted(paths)
```

---

### Questions?
